SET manPath=D:\Users\doudy\SDS\Monitoring\EmergingIssues\build\manifest.txt
SET libPath=D:\Users\doudy\SDS\Monitoring\EmergingIssues\lib
SET outRoot=D:\Users\doudy\SDS\Monitoring\EmergingIssues\classes
SET appPath=D:\Users\doudy\SDS\Monitoring\EmergingIssues\app

SET listHelper=D:\Users\doudy\SDS\Monitoring\EmergingIssues\build\listClasses.bat
SET currDir=%cd%
SET classList=classList.txt

cd %outRoot%

CALL %listHelper% >> %classList%

SET jar0=sendgrid-java.jar
SET jar1=gson-2.8.5.jar
SET jar2=vertica-jdbc-8.1.1-18.jar
SET jar3=commons-math3-3.6.1.jar

IF NOT EXIST %appPath%\lib\%jar0% COPY %libPath%\%jar0% %appPath%\lib
IF NOT EXIST %appPath%\lib\%jar1% COPY %libPath%\%jar1% %appPath%\lib
IF NOT EXIST %appPath%\lib\%jar2% COPY %libPath%\%jar2% %appPath%\lib
IF NOT EXIST %appPath%\lib\%jar3% COPY %libPath%\%jar3% %appPath%\lib

jar cmvf %manPath% %appPath%\EmergingIssues.jar @%classList%

del %classList%

cd %currDir%