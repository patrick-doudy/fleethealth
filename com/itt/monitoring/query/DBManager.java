package com.itt.monitoring.query;

import java.sql.*;
import java.util.*;

import com.itt.monitoring.cfg.MonitorConfig;

public class DBManager {

	private static Connection connection = null;

	private static DBManager singleton = new DBManager();

	private DBManager(){

		Properties connectionProperties = new Properties();
		connectionProperties.put("user", MonitorConfig.getDBConnUser());
		connectionProperties.put("password", MonitorConfig.getDBConnPass());

		try {

			connection = DriverManager.getConnection(
				MonitorConfig.getDBConnString(), 
				connectionProperties
			);

		} catch (SQLTransientConnectionException connectionException){

			System.out.println("Connection issue: ");
			System.out.println(connectionException.getMessage());
			System.out.println("Aborting Startup");
			System.exit(0);

		} catch (SQLException sqlException){

			System.out.println("SQL issue: ");
			System.out.println(sqlException.getMessage());
			System.out.println("Aborting Startup");
			System.exit(0);
		}
	}
	
	public static Connection getConnection() {

		return connection;
	}
}