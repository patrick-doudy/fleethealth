package com.itt.monitoring.util;

import java.util.*;
import java.sql.*;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TrendsUtil {
	
	private static final Calendar programCalendar = getRoundedCalendar(new java.util.Date());

	public static String stringFromFile(String filePath, Charset encoding) throws IOException {

		byte[] encodedString = Files.readAllBytes(Paths.get(filePath));

		return new String(encodedString, encoding);
	}

	public static Calendar cloneProgramCalendar(){

		return (Calendar) TrendsUtil.programCalendar.clone();
	}

	public static Calendar getRoundedCalendar(java.util.Date date){

		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTime(date);

		Calendar calendar2 = Calendar.getInstance();
		calendar2.clear();
		calendar2.set(calendar1.get(Calendar.YEAR),
					  calendar1.get(Calendar.MONTH),
					  calendar1.get(Calendar.DAY_OF_MONTH));

		return calendar2;
	}

	public static java.util.Date getRoundedDate(java.util.Date date){

		Calendar calendar = getRoundedCalendar(date);

		return calendar.getTime();
	}

	public static java.sql.Date sqlDateFromCalendar(Calendar calendar){

		java.util.Date utilDate = calendar.getTime();
		java.sql.Date  sqlDate  = new java.sql.Date(utilDate.getTime());

		return sqlDate;
	}

	public static String getTodayString(){

		String date = "";

		date += programCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US)+" ";
		date += programCalendar.get(Calendar.DAY_OF_MONTH)+" ";
		date += programCalendar.get(Calendar.YEAR);

		return date;
	}

	public static java.util.Date mondayOfWeek(java.util.Date date){

		Calendar calendar = getRoundedCalendar(date);

		switch (calendar.get(Calendar.DAY_OF_WEEK)){
			case Calendar.SUNDAY:
				calendar.add(Calendar.DATE,1);
			break;
			case Calendar.MONDAY:
				// do nothing
			break;
			case Calendar.TUESDAY:
				calendar.add(Calendar.DATE,-1);
			break;
			case Calendar.WEDNESDAY:
				calendar.add(Calendar.DATE,-2);
			break;
			case Calendar.THURSDAY:
				calendar.add(Calendar.DATE,-3);
			break;
			case Calendar.FRIDAY:
				calendar.add(Calendar.DATE,-4);
			break;
			case Calendar.SATURDAY:
				calendar.add(Calendar.DATE,-5);
			break;
		}

		return calendar.getTime();
	}

	public static java.util.Date firstMondayOfMonth(java.util.Date date){

		Calendar calendar = getRoundedCalendar(date);

		calendar.set(Calendar.DAY_OF_MONTH, 1);

		while(calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY)
			calendar.add(Calendar.DAY_OF_MONTH, 1);

		return calendar.getTime();
	}
}