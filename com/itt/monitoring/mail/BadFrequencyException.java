package com.itt.monitoring.mail;

public class BadFrequencyException extends Exception {

	public BadFrequencyException(){

		super();
	}

	public BadFrequencyException(String message){

		super(message);
	}

	public BadFrequencyException(String message, Throwable cause){

		super(message, cause);
	}

	public BadFrequencyException(Throwable cause){

		super(cause);
	}
}