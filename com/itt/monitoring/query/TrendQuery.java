package com.itt.monitoring.query;

import java.util.*;
import java.sql.*;

import com.itt.monitoring.trenddata.TrendBaseData;

public abstract class TrendQuery {
	
	public static final String column1Dummy = "__COLUMN1";

	private String queryString;

	public String getQuerySTring(){

		return queryString;
	}

	public abstract ArrayList<TrendBaseData> performQuery(
		Connection connection, java.sql.Date earliestDate) throws SQLException;

}