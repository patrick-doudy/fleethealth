package com.itt.monitoring.mail;

import com.sendgrid.*;
import java.util.Arrays;
import java.nio.charset.Charset;
import java.io.*;

import com.itt.monitoring.cfg.MonitorConfig;
import com.itt.monitoring.util.*;
import com.itt.monitoring.results.*;

public class SendGridClient {

	public static void sendLatestResults() {

		String emailText = buildLatestEmailText();

		String[] destinationEmails = Subscribers.getInstance().getDueEmails();

		for(int idx = 0; idx < destinationEmails.length; ++idx)
			sendToEmail(emailText, destinationEmails[idx]);

		Subscribers.getInstance().updateSubscriberFile(destinationEmails);
	}

	private static String buildLatestEmailText(){

		String emailText = "";

		emailText += "Trends can be reviewed at this url:\n";
		emailText += MonitorConfig.getReportUrl()+"\n\n";
		emailText += "Subscriptions can be managed at this url (inside the HP network):\n";
		emailText += MonitorConfig.getSubscribeUrl()+"\n\n";
		emailText += Results.getLatestSummary();

		return emailText;
	}

	private static void sendToEmail(String emailText, String email){

		Email from 		= new Email("FleetHealthMonitoring@intimetec.com");
		String subject 	= "Print Fleet Potential Emerging Issues";
		Email to 		= new Email(email);
		Content content = new Content("text/plain", emailText);
		Mail mail 		= new Mail(from, subject, to, content);

		SendGrid sg 	= new SendGrid(MonitorConfig.getSgApiKey());

		Request request = new Request();

		try {

			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());

			Response response = sg.api(request);

			System.out.println("\nMail status:");
			System.out.println(response.getStatusCode());
			System.out.println(response.getBody());
			System.out.println(response.getHeaders());

		} catch (IOException ioException){

			System.out.println("Could not email results to "+email);
			System.out.println(ioException.getMessage());
		}	
	}
}