package com.itt.monitoring.query;

import java.util.*;
import java.sql.*;

import com.itt.monitoring.trenddata.TrendBaseData;
import com.itt.monitoring.cfg.*;

import com.google.gson.*;

public class TwoDimensionCountByDateQuery extends TrendQuery {

	private static final String dimension1Dummy = "__DIMENSION1";
	private static final String dimension2Dummy = "__DIMENSION2";

	private String queryString =
		" SELECT "
			+" FACT_EVENT."+TwoDimensionCountByDateQuery.dimension1Dummy+", "
			+" FACT_EVENT."+TwoDimensionCountByDateQuery.dimension2Dummy+", "
			+" DATE(FACT_EVENT.EVENT_DTM), "
			+" COUNT(FACT_EVENT."+TwoDimensionCountByDateQuery.dimension2Dummy+") "
		+" FROM "
			+" PPS_DEVICE_QARI.FACT_EVENT "
		+" INNER JOIN "
			+" PPS_DEVICE_QARI.DIM_PRODUCT "
			+" ON "
			+" DIM_PRODUCT.PRODUCT_CODE = FACT_EVENT.PRODUCT_NUMBER "
		+" INNER JOIN "
			+" PPS_DEVICE_QARI.DIM_DEVICE "
			+" ON "
			+" DIM_DEVICE.SERIAL_NUMBER = FACT_EVENT.SERIAL_NUMBER "
		+" WHERE "
			+" FACT_EVENT."+TwoDimensionCountByDateQuery.dimension1Dummy+" IS NOT NULL "
			+" AND "
			+" FACT_EVENT."+TwoDimensionCountByDateQuery.dimension2Dummy+" IS NOT NULL "
			+" AND "
			+" DIM_DEVICE.REGION NOT IN ('EMEA','EMEA ','Asia Pacific','Asia Pacific ') "
			+" AND "
			+" DATE(FACT_EVENT.EVENT_DTM) <= CURRENT_DATE() "
			+" AND "
			+" DATE(FACT_EVENT.EVENT_DTM) >= ? "
			+" AND "
			+" CONCURRENCY_DESC = 'UNIQUE' "
			+" AND "
			+" (FACT_EVENT.SEVERITY = 'ERROR' OR FACT_EVENT.SEVERITY = 'FATAL') "
			+" AND "
			+" LEFT(FACT_EVENT.EVENT_CODE, 2) NOT IN ('44') "
			+" AND "
			+" LEFT(FACT_EVENT.EVENT_CODE, 8) NOT IN ('31.13.00','31.13.01','99.07.20') "
			+" AND "
			+" DIM_PRODUCT.PRODUCT_CATEGORY NOT IN ('PSG RETAIL SOLUTIONS',"
	        +"'PSG SERVICES COMMERCIAL','INDIGO','CONSUMER DESKTOPS','LARGE FORMAT PRODUCTION',"
	        +"'WORKSTATIONS','OTHER PSG CONSUMER','PSG SERVICES CONSUMER','THIN CLIENTS',"
	        +"'SCANNERS','DP SERVICES AND SOLUTIONS','LARGE FORMAT INDUSTRIAL',"
	        +"'REPLACEMENT PART AND CONSUMABLES','COMMERCIAL TABLETS','CONSUMER NOTEBOOKS',"
	        +"'REPLACEMENT PARTS','3D PRINTING','OTH PSG COMMERCIAL',"
	        +"'MOBILE COMMERCIAL SOLUTIONS','CONSUMER MOBILITY','COMMERCIAL DESKTOPS',"
	        +"'COMMERCIAL NOTEBOOKS','MARKETING OPTIMIZATION')"
        +" GROUP BY "
        	+" FACT_EVENT."+TwoDimensionCountByDateQuery.dimension1Dummy+" ,"
        	+" FACT_EVENT."+TwoDimensionCountByDateQuery.dimension2Dummy+", "
        	+" DATE(FACT_EVENT.EVENT_DTM) "
    	+" ORDER BY "
    		+" FACT_EVENT."+TwoDimensionCountByDateQuery.dimension1Dummy+" ASC, "
    		+" FACT_EVENT."+TwoDimensionCountByDateQuery.dimension2Dummy+" ASC, "
    		+" DATE(FACT_EVENT.EVENT_DTM) ASC"
		;

	private final int dimension1Column 	= 1;
	private final int dimension2Column 	= 2;
	private final int dateColumn 		= 3;
	private final int valueColumn 	 	= 4;

	private TwoDimensionCountByDateQuery(String dimension1, String dimension2){

		queryString = queryString.replace(
			TwoDimensionCountByDateQuery.dimension1Dummy, dimension1);

		queryString = queryString.replace(
			TwoDimensionCountByDateQuery.dimension2Dummy, dimension2);
	}

	/*
		Two Dimension Count by Date from JSON
	*/

	private class TwoDimensionCountByDateQueryJSON {

		public String dimension1;
		public String dimension2;
	}

	public static TwoDimensionCountByDateQuery fromJSON(JsonElement jsonIn) throws BadConfigException {

		final String objName = "parameters";
		final String self 	 = "RegressionSlope";

		ConfigValidator.checkIsObject(self, objName, jsonIn);

		JsonObject paramsJson = jsonIn.getAsJsonObject();

		ConfigValidator.checkKeyValue(self, objName, paramsJson, "dimension1", JsonType.PRIMITIVE);
		ConfigValidator.checkKeyValue(self, objName, paramsJson, "dimension2", JsonType.PRIMITIVE);

		Gson gson = new Gson();
		
		TwoDimensionCountByDateQueryJSON jsonTDCBDQ =
			gson.fromJson(jsonIn, TwoDimensionCountByDateQueryJSON.class);

		return new TwoDimensionCountByDateQuery(
			jsonTDCBDQ.dimension1,
			jsonTDCBDQ.dimension2
		);
	}

	/*
		Query Methods
	*/

	@Override
	public ArrayList<TrendBaseData> performQuery(
		Connection connection, java.sql.Date earliestDate) throws SQLException {

		/*
			Acquire Results from DB
		*/

		ResultSet resultSet = null;

		PreparedStatement statement = connection.prepareStatement(this.queryString);

		statement.setDate(1, earliestDate);

		resultSet = statement.executeQuery();

		/*
			Parse
		*/

		String dimension1 = "";
		String dimension2 = "";

		ArrayList<java.sql.Date> trendDates  = null;
		ArrayList<Number> 		 trendValues = null;

		TrendBaseData trendBaseData = null;

		ArrayList<TrendBaseData> baseDataList = new ArrayList<TrendBaseData>();

		while(resultSet.next()){

			/*
				Detect data for next trend by comparing
				to see if sku or code for this row is
				different from the last - then finalize
				trend and add to container map
			*/

			if(!resultSet.getString(dimension1Column).equals(dimension1)
			|| !resultSet.getString(dimension2Column).equals(dimension2)){

				/*
					null trendBaseData indicates this
					is the first data group
				*/

				if(trendBaseData != null){

					trendBaseData.seed(trendDates, trendValues);
					baseDataList.add(trendBaseData);
				}

				dimension1 = resultSet.getString(dimension1Column);
				dimension2 = resultSet.getString(dimension2Column);

				trendDates  = new ArrayList<java.sql.Date>();
				trendValues = new ArrayList<Number>();

				trendBaseData = new TrendBaseData(dimension1, dimension2);
			}

			/*
				gather row data for current dimensions
			*/

			trendDates.add(resultSet.getDate(dateColumn));
			trendValues.add(resultSet.getInt(valueColumn));
		}

		/*
			Loop will break without storing
			data for last trend
		*/

		if(trendBaseData != null){

			trendBaseData.seed(trendDates, trendValues);
			baseDataList.add(trendBaseData);
		}

		statement.close();

		return baseDataList;
	}
}