package com.itt.monitoring.mail;

import java.io.*;
import java.util.Scanner;
import java.util.Date;
import java.util.ArrayList;
import java.nio.file.*;

import com.itt.monitoring.util.*;
import com.itt.monitoring.cfg.*;

public class Subscribers {

	private static Subscribers instance = new Subscribers();

	private Subscribers(){

	}

	public static Subscribers getInstance(){

		return instance;
	}

	public String[] getDueEmails() {

		ArrayList<Subscriber> subscriberList = readSubscribers();

		for(int idx = 0; idx < subscriberList.size(); ++idx){

			Subscriber subscriber = subscriberList.get(idx);

			if(!subscriber.isDue()){

				System.out.println(subscriber.getEmail()+" is not due for report");

				subscriberList.remove(idx);
				--idx;

			} else {

				System.out.println(subscriber.getEmail()+" is due for report");
			}
		}

		String[] emails = new String[subscriberList.size()];
		
		for(int idx = 0; idx < subscriberList.size(); ++idx)
			emails[idx] = subscriberList.get(idx).getEmail();

		return emails;
	}

	public void updateSubscriberFile(String[] emailsToUpdate){

		ArrayList<Subscriber> inputList 	= readSubscribers();
		ArrayList<Subscriber> outputList	= new ArrayList<Subscriber>();

		Date today = TrendsUtil.cloneProgramCalendar().getTime();

		// Change time stamps of subscribers in the input to array
		// to the current date

		for(int idx1 = 0; idx1 < emailsToUpdate.length; ++idx1){

			String email = emailsToUpdate[idx1];

			for(int idx2 = 0; idx2 < inputList.size(); ++idx2){

				Subscriber subscriber = inputList.get(idx2);

				if(email.equals(subscriber.getEmail())){

					subscriber.setDate(today);
					outputList.add(subscriber);
					inputList.remove(idx2);
					--idx2;
				}
			}
		}

		// Combine updated and non-updated into a complete list

		while(!inputList.isEmpty()){
			outputList.add(inputList.get(0));
			inputList.remove(0);
		}

		// Replace the subscribers file with an updated version

		try {

			String tempFileName = "tempJava.csv";

			PrintWriter writer = new PrintWriter(
				MonitorConfig.getSubscribersDir()+tempFileName,
				"UTF-8"
			);

			for(int idx = 0; idx < outputList.size(); ++idx){

				Subscriber subscriber = outputList.get(idx);
				writer.write(subscriber.getEmail()+", ");
				writer.write(subscriber.getFrequency().toString()+", ");
				writer.write(subscriber.getDateLastSent().getTime()+"\r\n");
			}

			writer.close();

			Files.deleteIfExists(new File(MonitorConfig.getSubscribersFilePath()).toPath());
			Files.move(
				new File(MonitorConfig.getSubscribersDir()+tempFileName).toPath(),
				new File(MonitorConfig.getSubscribersFilePath()).toPath(),
				java.nio.file.StandardCopyOption.REPLACE_EXISTING
			);

		} catch (IOException e){

			System.out.println("could not update subscriber list");
			System.out.println(e.getMessage());
			System.exit(0);
		}
	}

	private ArrayList<Subscriber> readSubscribers() {

		ArrayList<Subscriber> subscriberList = new ArrayList<Subscriber>();

		try {

			Scanner fscanner = new Scanner(new File(MonitorConfig.getSubscribersFilePath()));
			Scanner lscanner;

			String line, email, freqStr, dateStr;

			while(fscanner.hasNextLine()){

				line 		= fscanner.nextLine();
				lscanner	= new Scanner(line);

				lscanner.useDelimiter(",");

				email		= lscanner.next().trim();
				freqStr 	= lscanner.next().trim();
				dateStr 	= lscanner.next().trim();

				subscriberList.add(new Subscriber(email, freqStr, dateStr));

				lscanner.close();
			}
			
			fscanner.close();

		} catch (IOException e){

			System.out.println("Could not parse subscribers");
			System.out.println(e.getMessage());
			System.exit(0);

		} catch (BadFrequencyException e){

			System.out.println("Could not parse subscribers");
			System.out.println(e.getMessage());
			System.exit(0);
		}

		return subscriberList;		
	}

	private class Subscriber {

		private String 			email;
		private MailFrequency	frequency;
		private Date 			dateLastSent;

		public Subscriber(String email, String freqStr, String dateStr)
			throws BadFrequencyException {

			this.email 			= email;
			this.frequency 		= MailFrequency.fromString(freqStr);
			this.dateLastSent 	= TrendsUtil.getRoundedDate(
									new Date(
										Long.parseLong(dateStr)
									)
								);
		}

		public String getEmail(){

			return email;
		}

		public MailFrequency getFrequency(){

			return frequency;
		}

		public Date getDateLastSent(){

			return dateLastSent;
		}

		public void setDate(Date date){

			dateLastSent = date;
		}

		public boolean isDue(){

			Date today = TrendsUtil.cloneProgramCalendar().getTime();

			if(frequency == MailFrequency.DAILY){

				return dateLastSent.before(today);

			} else if (frequency == MailFrequency.FIRST_MONDAY_WEEK){

				return dateLastSent.before(TrendsUtil.mondayOfWeek(today));

			} else if (frequency == MailFrequency.FIRST_MONDAY_MONTH){

				return dateLastSent.before(TrendsUtil.firstMondayOfMonth(today));
			}

			return false;
		}
	}
}