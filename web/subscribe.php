<?php

require 'subscribe_util.php';

function format_freq($inFreq){

	return strtoupper(
			sanitize_input(
				$inFreq
			)
		);
}

function get_freq_err($postData){

	if(empty($postData["frequency"]))
		return 1;

	$inFreq = format_freq($postData["frequency"]);

	switch($inFreq){
		case "DAILY":
		case "FIRST_MONDAY_WEEK":
		case "FIRST_MONDAY_MONTH":
			return 0;
		break;
		default:
			return 2;
		break;
	}
}

function valid_request($postData){

	$emailErr = get_email_err($postData);
	$freqErr  = get_freq_err($postData);

	return ($emailErr == 0 && $freqErr == 0);
}

function add_subscriber($email, $freq, $subscribers){

	$existing = false;

	for($row = 0; $row < count($subscribers); ++$row){
		if($subscribers[$row][COL_EMAIL] == $email){
			$existing = true;
			$subscribers[$row][COL_FREQ] = $freq;
			$subscribers[$row][COL_DATE] = 0;
			break;
		}
	}

	if(!$existing){
		$newRow = count($subscribers);
		$subscribers[$newRow][COL_EMAIL] = $email;
		$subscribers[$newRow][COL_FREQ]  = $freq;
		$subscribers[$newRow][COL_DATE]  = 0;
	}

	return $subscribers;
}

function file_actions($email, $freq){

	if(($subscribers = read_subscribers()) !== false){

		$subscribers = add_subscriber($email, $freq, $subscribers);

		if(!write_subscribers($subscribers, get_sub_temp_path(SUB_TMP_FN)))
			return 2;
		if(!rotate_files(get_sub_temp_path(SUB_TMP_FN), get_subscribers_path()))
			return 3;

	} else {

		return 1;
	}

	return 0;
}

if($_SERVER["REQUEST_METHOD"] == "POST"){

	$emailErr = 0;
	$freqErr  = 0;
	$fileErr  = 0;

	if(valid_request($_POST)){

		$email = $_POST["email"];
		$freq  = format_freq($_POST["frequency"]);

		$fileErr = file_actions($email, $freq);

	} else {

		$emailErr = get_email_err($_POST);
		$freqErr  = get_freq_err($_POST);
	}

	$success = (   $emailErr == 0
			     && $freqErr == 0
				 && $fileErr == 0
				);

	if($success)
		echo(CODE_SUCCESS);
	else
		echo(CODE_FAIL.$emailErr.$freqErr.$fileErr);
}

?>