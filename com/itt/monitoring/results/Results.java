package com.itt.monitoring.results;

import java.util.Arrays;
import java.util.Calendar;
import java.nio.charset.Charset;
import java.io.*;

import com.itt.monitoring.cfg.*;
import com.itt.monitoring.util.*;

public class Results {
	
	public static String getLatestSummary(){

		String fileName = String.valueOf(getLatestReport())+".txt";
		String filePath = MonitorConfig.getAnalysesDir() + fileName;

		String report = "";

		try {

			report = TrendsUtil.stringFromFile(filePath, Charset.forName("UTF-8"));

		} catch(IOException e) {

			System.out.println("Could not read latest summary");
			System.out.println(e.getMessage());
			System.exit(0);
		}

		System.out.println("\n\nlatest summary located in "+fileName);

		return report;
	}

	private static long[] getDateOrderedReports(){

		File 	analysesFolder 	= new File(MonitorConfig.getAnalysesDir());
		File[] 	reports 		= analysesFolder.listFiles();
		long[] 	stamps 			= new long[reports.length];

		for(int idx = 0; idx < reports.length; ++idx){

			stamps[idx] = Long.parseLong((reports[idx].getName().replaceAll("[^0-9]","")));
		}

		Arrays.sort(stamps);

		return stamps;	
	}

	private static long getLatestReport(){

		long[] stamps = getDateOrderedReports();

		return stamps[stamps.length -1];
	}

	public static void saveResult(String summary){

		String path 			= MonitorConfig.getAnalysesDir();
		String dateMilliseconds = new Long(new java.util.Date().getTime()).toString();
		String fileName 		= path + dateMilliseconds + ".txt";

		try {

			Writer writer =
				new BufferedWriter(
					new OutputStreamWriter(
						new FileOutputStream(fileName),
						"utf-8"
					)
				);

			writer.write(summary);
			writer.close();

		} catch (IOException e){

			System.out.println("Could not write analysis to disc");
			System.out.println(e.getMessage());
		}
	}

	public static void cleanResults(){

		long[] 		reports = getDateOrderedReports();
		boolean[] 	retain  = new boolean[reports.length];

		Arrays.fill(retain, true);

		Calendar limitCalendar = TrendsUtil.cloneProgramCalendar();

		limitCalendar.add(
			Calendar.DATE, 
			-MonitorConfig.getReportDaysRetain()
		);

		long limitStamp = limitCalendar.getTimeInMillis();

		// mark for deletion reports falling before
		// retention span limit
		for(int idx = 0; idx < reports.length; ++idx){

			// only switch from true to false
			if(retain[idx])
				retain[idx] = (reports[idx] >= limitStamp);

			if(retain[idx])
				System.out.println(String.valueOf(reports[idx])+" inside date span");
			else
				System.out.println(String.valueOf(reports[idx])+" outside date span");
		}
		
		// mark oldest reports for deletion when count
		// exceeds maximum number to retain
		if(reports.length > MonitorConfig.getMaxReportsRetain()){

			int limitIdx = reports.length -MonitorConfig.getMaxReportsRetain();

			for(int idx = 0; idx < reports.length; ++idx){

				// only switch from true to false
				if(retain[idx])
					retain[idx] = (idx >= limitIdx);

				if(retain[idx])
					System.out.println(String.valueOf(reports[idx])+" inside count limit");
				else
					System.out.println(String.valueOf(reports[idx])+" outside count limit");
			}
		}

		// remove reports marked for deletion
		for(int idx = 0; idx < retain.length; ++idx){

			if(!retain[idx]){

				String fileName = String.valueOf(reports[idx])+".txt";

				File file = new File(MonitorConfig.getAnalysesDir()+fileName);

				file.delete();

				System.out.println(String.valueOf(reports[idx])+" deleted");

			} else {

				System.out.println(String.valueOf(reports[idx])+" preserved");
			}
		}
	}
}