package com.itt.monitoring.metric;

import java.sql.*;
import java.util.*;
import java.lang.Math;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import com.itt.monitoring.trenddata.TrendBaseData;
import com.itt.monitoring.util.TrendsUtil;
import com.itt.monitoring.cfg.*;

import org.apache.commons.math3.stat.regression.SimpleRegression;

import com.google.gson.*;

public class MovingAverageDelta extends Metric {
	
	private int longWindow;
	private int shortWindow;
	private int trendDays;
	private int daysOffset;
	private int totalDays;
	private int numToReport;

	private double minAvgCountPerDate;
	private double allowableNullFraction;

	private ArrayList<MADTrend> worstOffenders = null;

	private Number inflateNullValue = new Integer(0);

	private MovingAverageDelta (
		int shortWindow, int longWindow, 
		int trendDays, int numToReport,
		double allowableNullFraction,
		double minAvgCountPerDate) throws BadConfigException {

		if(!(shortWindow > 0 && longWindow > 0 && trendDays > 0 && numToReport > 0))
			throw new BadConfigException("MovingAverageDetla shortWindow, "
				+"longWindow, trendDays, and numToReport "
				+"must all be greater than zero");

		if(!(shortWindow < longWindow && longWindow < trendDays))
			throw new BadConfigException("MovingAverageDelta shortWindow "
				+"must be less than longWindow, longWindow must be less "
				+"than trendDays");

		if(!(allowableNullFraction >= 0.0 && allowableNullFraction < 1.0))
			throw new BadConfigException("MovingAverageDelta allowableNullFraction "
				+"must be greater than or equal to zero and less than one");

		if(!(minAvgCountPerDate >= 0.0))
			throw new BadConfigException("MovingAverageDelta minAvgCountPerDate "
				+"must be greater than or equal to zero");

		this.shortWindow  = shortWindow;
		this.longWindow   = longWindow;
		this.trendDays 	  = trendDays;
		this.numToReport  = numToReport;

		this.allowableNullFraction  = allowableNullFraction;
		this.minAvgCountPerDate  	= minAvgCountPerDate;

		this.daysOffset   = longWindow -1;
		this.totalDays    = trendDays + daysOffset;

		this.worstOffenders = new ArrayList<MADTrend>();
	}

	/*
		Moving Average Delta from JSON
	*/

	private class MovingAverageDeltaJSON {

		public int shortWindow;
		public int longWindow;
		public int trendDays;
		public int numToReport;
		public double allowableNullFraction;
		public double minAvgCountPerDate;
	}

	public static MovingAverageDelta fromJSON(JsonElement jsonIn) throws BadConfigException {

		final String objName = "parameters";
		final String self    = "MovingAverageDelta";

		ConfigValidator.checkIsObject(self, objName, jsonIn);

		JsonObject paramsJson = jsonIn.getAsJsonObject();

		ConfigValidator.checkKeyValue(self, objName, paramsJson, "shortWindow", JsonType.PRIMITIVE);
		ConfigValidator.checkKeyValue(self, objName, paramsJson, "longWindow", JsonType.PRIMITIVE);
		ConfigValidator.checkKeyValue(self, objName, paramsJson, "trendDays", JsonType.PRIMITIVE);
		ConfigValidator.checkKeyValue(self, objName, paramsJson, "numToReport", JsonType.PRIMITIVE);
		ConfigValidator.checkKeyValue(self, objName, paramsJson, "allowableNullFraction", JsonType.PRIMITIVE);
		ConfigValidator.checkKeyValue(self, objName, paramsJson, "minAvgCountPerDate", JsonType.PRIMITIVE);

		Gson gson = new Gson();
		MovingAverageDeltaJSON jsonMAD = gson.fromJson(jsonIn, MovingAverageDeltaJSON.class);

		return new MovingAverageDelta(
			jsonMAD.shortWindow,
			jsonMAD.longWindow,
			jsonMAD.trendDays,
			jsonMAD.numToReport,
			jsonMAD.allowableNullFraction,
			jsonMAD.minAvgCountPerDate
		);
	}

	/*
		Metric Methods
	*/

	@Override
	public int getDaysOffset() {

		return this.totalDays;
	}

	@Override
	public java.sql.Date getEarliestDate() {

		Calendar calendar = TrendsUtil.cloneProgramCalendar();

		calendar.add(Calendar.DATE, - this.totalDays);

		return TrendsUtil.sqlDateFromCalendar(calendar);
	}

	@Override
	public Number getInflateNullValue(){

		return this.inflateNullValue;
	}

	@Override
	public void evaluate(ArrayList<TrendBaseData> baseDataList){

		for(int idx = 0; idx < baseDataList.size(); ++idx){

			TrendBaseData baseData = null;

			MADTrend madTrend = null;

			baseData = baseDataList.get(idx);

			baseData.inflate(
				this.getDaysOffset(),
				this.getInflateNullValue()
			);

			/*
				Evaluate this trend only if it has a worthwhile
				amount of information to display and has an
				increasing slope to its trend
			*/

			if(baseData.getNullFraction() > allowableNullFraction
			 ||baseData.getAvgCountPerDate() < minAvgCountPerDate){

				baseDataList.set(idx, null);
				baseData = null;
				madTrend = null;

				continue;
			}

			madTrend = new MADTrend(
				this.shortWindow,
				this.longWindow,
				this.trendDays,
				this.daysOffset,
				baseData
			);

			if (madTrend.getNormalizedSlope() <= 0.0){

				baseDataList.set(idx, null);
				baseData = null;
				madTrend = null;

				continue;
			}

			/*
				If this instance is not a worst offender
				allow garbage collection
			*/

			if(storeIfWorstOffender(madTrend)){

				// madTrend.writeCSV();

			} else {

				baseDataList.set(idx,null);
				madTrend = null;
			}
		}

		Collections.sort(worstOffenders, Collections.reverseOrder());
	}

	@Override
	public void printWorstOffenders(){

		System.out.print(getWorstOffenderSummary());
	}

	@Override
	public String getWorstOffenderSummary(){

		String message = "Moving Average Delta Worst "
			+numToReport+" Offenders Last "
			+trendDays+" Days:\r\n\r\n";

		for(int idx = 0; idx < worstOffenders.size(); ++idx){

			MADTrend offender = worstOffenders.get(idx);

			message += offender.getDimensionName()+"\r\n";

			// message += "DIM1: "+offender.getDimension1()
			// 	+"DIM2: "+offender.getDimension2()
			// 	+" Normalized Delta: "+offender.getNormalizedDelta()
			// 	+" Nominal Delta: "+offender.getNominalDelta()
			// 	+" Average per Date: "+offender.getAvgCountPerDate()
			// 	+"\n";
		}

		return message;
	}

	private boolean storeIfWorstOffender(MADTrend madTrend){

		if(worstOffenders.size() < numToReport){

			worstOffenders.add(madTrend);

			return true;

		} else {

			int 	minIdx   = -1;
			double  minDelta = Double.MAX_VALUE;

			for(int idx = 0; idx < worstOffenders.size(); ++idx){

				double delta = worstOffenders.get(idx).getAbsNormDelta();

				if(delta < minDelta){
					minDelta = delta;
					minIdx   = idx;
				}
			}

			if(madTrend.getAbsNormDelta() > minDelta){

				worstOffenders.set(minIdx, madTrend);

				return true;
			}
		}

		return false;
	}

	private class MADTrend implements Comparable<MADTrend> {

		private String dimension1 = "";
		private String dimension2 = "";

		private int shortWindow;
		private int longWindow;
		private int trendDays;
		private int daysOffset;

		private double avgCountPerDate 		= 0.0;

		private java.sql.Date[] dates 		= null;
		private double[] nominalValues 		= null;
		private double[] normalizedValues 	= null;

		private double[] nominalSMAShort 	= null;
		private double[] nominalSMALong 	= null;
		private double[] normalizedSMAShort = null;
		private double[] normalizedSMALong  = null;

		private double[] nominalSMADelta 	= null;
		private double[] normalizedSMADelta = null;

		private double   totalNormalizedDelta = 0.0;
		private double 	 absTotalNormDelta    = 0.0;

		private double 	 normalizedSlope 	= 0.0;

		public MADTrend(
			int shortWindow, int longWindow, 
			int trendDays, int daysOffset,
			TrendBaseData baseData){

			this.dimension1 = baseData.getDimension1();
			this.dimension2 = baseData.getDimension2();

			this.shortWindow = shortWindow;
			this.longWindow  = longWindow;
			this.trendDays   = trendDays;
			this.daysOffset  = daysOffset;

			this.avgCountPerDate = baseData.getAvgCountPerDate();

			/*
				copy out nominal data while
				computing normalized data
			*/

			int baseEntries 			= baseData.getBaseDates().length;
			java.sql.Date[] baseDates 	= baseData.getBaseDates();
			Number[] baseValues 		= baseData.getBaseValues();

			dates 			 = new java.sql.Date[baseEntries];
			nominalValues 	 = new double[baseEntries];
			normalizedValues = new double[baseEntries];

			double nominalMax = -Double.MAX_VALUE;

			for(int idx = 0; idx < baseEntries; ++idx){

				dates[idx] 			= baseDates[idx];
				nominalValues[idx] 	= baseValues[idx].doubleValue();

				if(nominalValues[idx] > nominalMax && nominalValues[idx] > 0.0)
					nominalMax = nominalValues[idx];
			}

			if(nominalMax == -Double.MAX_VALUE)
				nominalMax = 1.0;

			for(int idx = 0; idx < baseEntries; ++idx)
				normalizedValues[idx] = nominalValues[idx] / nominalMax;

			/*
				calculated short/long nominal/normalized simple
				moving averages while tracking sum(abs(normalized delta))
			*/

			nominalSMAShort 	= new double[trendDays];
			nominalSMALong  	= new double[trendDays];
			normalizedSMAShort	= new double[trendDays];
			normalizedSMALong 	= new double[trendDays];

			nominalSMADelta 	= new double[trendDays];
			normalizedSMADelta 	= new double[trendDays];

			for(int idx = trendDays -1; idx >= 0; --idx){

				int baseDataIdx = daysOffset + idx;

				nominalSMAShort[idx] 	= calculateSMAEntry(nominalValues, baseDataIdx, shortWindow);
				nominalSMALong[idx]  	= calculateSMAEntry(nominalValues, baseDataIdx, longWindow);
				normalizedSMAShort[idx]	= calculateSMAEntry(normalizedValues, baseDataIdx, shortWindow);
				normalizedSMALong[idx]	= calculateSMAEntry(normalizedValues, baseDataIdx, longWindow);

				nominalSMADelta[idx] 	= nominalSMAShort[idx]    - nominalSMALong[idx];
				normalizedSMADelta[idx]	= normalizedSMAShort[idx] - normalizedSMALong[idx];

				totalNormalizedDelta += normalizedSMADelta[idx];
				absTotalNormDelta 	 += Math.abs(normalizedSMADelta[idx]);
			}

			/*
				Compute slope of normalized data using simple regression
			*/

			SimpleRegression regression = new SimpleRegression();

			for(int idx = 0; idx < baseEntries; ++idx)
				regression.addData(idx, normalizedValues[idx]);

			normalizedSlope = regression.getSlope();

			regression = null;
		}

		@Override
		public int compareTo(MADTrend other){

			double deltaOther = other.getAbsNormDelta();

			if(absTotalNormDelta > deltaOther)
				return 1;
			else if (absTotalNormDelta == deltaOther)
				return 0;
			else
				return -1;
		}

		public String getDimension1(){

			return dimension1;
		}

		public String getDimension2(){

			return dimension2;
		}

		public double getNormalizedSlope(){

			return normalizedSlope;
		}

		public double getAbsNormDelta(){

			return absTotalNormDelta;
		}

		public double getAvgCountPerDate(){

			return avgCountPerDate;
		}

		private double calculateSMAEntry(double[] sourceData, int startIdx, int window){

			double sum = 0;

			for(int iterations = 0; iterations < window; ++iterations)
				sum += sourceData[startIdx - iterations];

			return (sum / window);
		}

		public double getPercChange(){

			/*
				Take appx percentage change as ratio of max(nominalSMADelta)
				to min(*nonzero nominalSMADelta)
			*/

			double[] copy = Arrays.copyOf(nominalSMADelta, nominalSMADelta.length);

			for(int idx = 0; idx < copy.length; ++idx)
				copy[idx] = (copy[idx] < 0.0) ? 0.0 : copy[idx];

			Arrays.sort(copy);

			double max = copy[copy.length -1];
			double min = 0.0;

			for(int idx = 0; idx < copy.length; ++idx){
				if(copy[idx] > 0.0){
					min = copy[idx];
					break;
				}
			}

			double delta =  (max != 0.0)
						  ? (1 - min / max)
						  : (1 - min / 1.0);

			return Math.floor(100 * delta);
		}

		public String getDimensionName(){

			return (dimension1+" vs "+dimension2);
		}

		public String getPercChangeMsg(){

			String message = dimension1+" vs "+dimension2
				+" "+getPercChange()+"% change";

			return message;
		}

		public void writeCSV(){

			String fileName = dimension1+"vs"+dimension2+".csv";

			try {

				File csvFile = new File(fileName);

				csvFile.delete();

				PrintWriter printWriter = new PrintWriter(csvFile);

				StringBuilder builder = new StringBuilder();

				builder.append("date, nominalVal, normalizedVal, nomSma"+shortWindow+", "
					+"nomSma"+longWindow+", normSma"+shortWindow+", normSma"+longWindow+"\n");

				for(int offset = 0, baseIdx = dates.length-1, trendIdx = nominalSMAShort.length-1; 
					trendDays -offset > 0; 
					++offset, --baseIdx, --trendIdx){

					builder.append(dates[baseIdx].toString()+", "+nominalValues[baseIdx]+", "
						+normalizedValues[baseIdx]+", "+nominalSMAShort[trendIdx]+", "
						+nominalSMALong[trendIdx]+", "+normalizedSMAShort[trendIdx]+", "
						+normalizedSMALong[trendIdx]+"\n");
				}

				printWriter.write(builder.toString());

				printWriter.close();

			} catch (FileNotFoundException FNFException) {

				System.out.println("Could not write csv for "+fileName);

				return;
			}
		}
	}
}