package com.itt.monitoring.trenddata;

import java.sql.*;
import java.util.*;

import com.itt.monitoring.util.TrendsUtil;

public class TrendBaseData {

	private String dimension1 = "";
	private String dimension2 = "";

	private java.sql.Date[] baseDates  = null;
	private Number[] 		baseValues = null;

	private double 	baseTotal  		= 0;
	private double  avgCountPerDate 		= 0.0;
	private int 	nullCount 		= 0;
	private double 	nullFraction 	= 0.0; 

	private HashMap<java.sql.Date, Number> seedData = null;

	public TrendBaseData(String dimension1, String dimension2){

		this.dimension1 = dimension1;
		this.dimension2 = dimension2;
	}

	public String getDimension1(){

		return dimension1;
	}

	public String getDimension2(){

		return dimension2;
	}

	public java.sql.Date[] getBaseDates(){

		return baseDates;
	}

	public Number[] getBaseValues(){

		return baseValues;
	}

	public int getNullCount(){

		return nullCount;
	}

	public double getNullFraction(){

		return nullFraction;
	}

	public double getAvgCountPerDate(){

		return avgCountPerDate;
	}

	public void seed(ArrayList<java.sql.Date> dates, ArrayList<Number> values){

		seedData = new HashMap<java.sql.Date, Number>();

		for(int idx = 0; idx < dates.size(); ++idx)
			seedData.put(dates.get(idx), values.get(idx));
	}

	public void inflate(int numDays, Number nullValue){

		this.baseDates  = new java.sql.Date[numDays];
		this.baseValues = new Number[numDays];

		Calendar calendar = TrendsUtil.cloneProgramCalendar();

		calendar.add(Calendar.DATE, -numDays);

		java.util.Date utilDate;
		java.sql.Date  keyDate;

		for(int idx = 0; idx < numDays; ++idx, calendar.add(Calendar.DATE, 1)){

			utilDate = calendar.getTime();
			keyDate  = new java.sql.Date(utilDate.getTime());

			baseDates[idx]  = keyDate;

			if(seedData.get(keyDate) != null){

				baseValues[idx] = seedData.get(keyDate);
				baseTotal += baseValues[idx].doubleValue();

			} else {

				baseValues[idx] = nullValue;
				++nullCount;
			}

			// if(seedData.get(keyDate) != null)
			// 	System.out.println("inflate idx "+idx+" keyDate "
			// 		+keyDate.toString()+" value "
			// 		+baseValues[idx].toString());
		}

		nullFraction = ((double) nullCount) / ((double) numDays);

		avgCountPerDate   = baseTotal / ((double) numDays);

		//System.out.println("numDays: "+numDays+" nullCount: "+nullCount+" nullFraction: "+nullFraction);

		// seed data no longer needed,
		// allow garbage collection
		this.seedData = null;
	}
}