package com.itt.monitoring.cfg;

public class BadConfigException extends Exception {

	public BadConfigException(){

		super();
	}

	public BadConfigException(String message){

		super(message);
	}

	public BadConfigException(String message, Throwable cause){

		super(message, cause);
	}

	public BadConfigException(Throwable cause){

		super(cause);
	}
}