package com.itt.monitoring.metric;

import java.util.*;
import java.sql.*;

import com.itt.monitoring.trenddata.TrendBaseData;

public abstract class Metric {

	private Number inflateNullValue;

	public abstract java.sql.Date getEarliestDate();

	public abstract int getDaysOffset();

	public abstract Number getInflateNullValue();

	public abstract void evaluate(ArrayList<TrendBaseData> baseDataList);

	public abstract void printWorstOffenders();

	public abstract String getWorstOffenderSummary();
}