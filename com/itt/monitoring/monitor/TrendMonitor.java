package com.itt.monitoring.monitor;

import java.sql.*;
import java.util.*;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

import com.itt.monitoring.cfg.*;
import com.itt.monitoring.query.*;
import com.itt.monitoring.trenddata.*;
import com.itt.monitoring.mail.*;
import com.itt.monitoring.results.*;

public class TrendMonitor {

	private static final int nonHiddenCfgPathIdx = 0;
	private static final int hiddenCfgPathIdx    = 1;
	private static final int trendConfigPathIdx  = 2;
	private static final int actionIdx 			 = 3;
	private static final int expectedArgs 		 = 4;

	private static final String actionAnalyze 	= "analyze";
	private static final String actionEmail 	= "email";
	private static final String actionClean 	= "clean";
	private static final String actionAll 		= "all";

	private static String nonHiddenCfgPath 		= "";
	private static String hiddenCfgPath 		= "";
	private static String trendConfigPath 		= "";
	private static String action 				= "";

	public static void main(String[] args){

		if(args.length != expectedArgs){

			System.out.println(
				"incorrect argument count, expected "
				+expectedArgs+", found "+args.length);
			System.out.println("Aborting Startup");
			System.exit(0);
		}

		if(nonHiddenCfgPathIdx < args.length)
			nonHiddenCfgPath 	= args[nonHiddenCfgPathIdx];

		if(hiddenCfgPathIdx < args.length)
			hiddenCfgPath = args[hiddenCfgPathIdx];

		if(trendConfigPathIdx < args.length)
			trendConfigPath = args[trendConfigPathIdx];

		if(actionIdx < args.length)
			action 			= args[actionIdx];

		System.out.println("Non hidden configs will be read from: "+nonHiddenCfgPath);
		System.out.println("Hidden configs will be read from: "+hiddenCfgPath);
		System.out.println("Trend configs will be read from: "+trendConfigPath);
		System.out.println("Report URL Location: "+MonitorConfig.getReportUrl());
		System.out.println("Analyses output directory: "+MonitorConfig.getAnalysesDir());
		System.out.println("Subscriber file location: "+MonitorConfig.getSubscribersFilePath());
		System.out.println("Action request: "+action);

		MonitorConfig.readAppConfigs(nonHiddenCfgPath, hiddenCfgPath);

		if(action.equals(actionAnalyze)
		|| action.equals(actionAll)){

			TrendsContainer.readTrendConfigs(trendConfigPath);

			TrendsContainer.computeTrends(
				DBManager.getConnection()
			);

			Results.saveResult(TrendsContainer.buildSummaryMessage());
		}

		if(action.equals(actionEmail)
		|| action.equals(actionAll)){

			SendGridClient.sendLatestResults();
		}

		if(action.equals(actionClean)
		|| action.equals(actionAll)){

			Results.cleanResults();
		}

		System.out.println("\nDone");
	}
}