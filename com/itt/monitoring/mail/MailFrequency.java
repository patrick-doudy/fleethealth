package com.itt.monitoring.mail;

public enum MailFrequency {

	DAILY,
	FIRST_MONDAY_WEEK,
	FIRST_MONDAY_MONTH;

	private static final String dailyStr 		= "DAILY";
	private static final String firstMonWkStr 	= "FIRST_MONDAY_WEEK";
	private static final String firstMonMoStr 	= "FIRST_MONDAY_MONTH";

	public static MailFrequency fromString(String frequency)
		throws BadFrequencyException {

		String upper = frequency.toUpperCase();

		if(upper.equals(dailyStr))
			return DAILY;
		else if (upper.equals(firstMonWkStr))
			return FIRST_MONDAY_WEEK;
		else if (upper.equals(firstMonMoStr))
			return FIRST_MONDAY_MONTH;
		else
			throw new BadFrequencyException("no such frequency "+frequency);
	}
}	