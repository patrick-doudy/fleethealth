<?php

require 'subscribe_util.php';

function valid_request($postData){

	$emailErr = get_email_err($postData);

	return($emailErr == 0);
}

function remove_subscriber($email, $subscribers){

	for($row = 0; $row < count($subscribers); ++$row){

		if($subscribers[$row][COL_EMAIL] == $email){

			for($col = 0; $col < SUB_FIELDS; ++$col){
				unset($subscribers[$row][$col]);
			}

			unset($subscribers[$row]);
			$subscribers = array_values($subscribers);

			return $subscribers;
		}
	}

	return null;
}

function file_actions($email){

	if(($subscribers = read_subscribers()) !== false){

		if(!subscriber_exists($email, $subscribers))
			return 2;
		if(null === ($subscribers = remove_subscriber($email, $subscribers)))
			return 3;
		if(!write_subscribers($subscribers, get_sub_temp_path(UNSUB_TMP_FN)))
			return 4;
		if(!rotate_files(get_sub_temp_path(UNSUB_TMP_FN), get_subscribers_path()))
			return 5;

	} else {

		return 1;
	}

	return 0;
}

if($_SERVER["REQUEST_METHOD"] == "POST"){

	$emailErr = 0;
	$fileErr  = 0;

	if(valid_request($_POST)){

		$email = $_POST["email"];

		$fileErr = file_actions($email);

	} else {

		$emailErr = get_email_err($_POST);
	}

	$success = ($emailErr == 0 && $fileErr == 0);

	if($success)
		echo(CODE_SUCCESS);
	else
		echo(CODE_FAIL.$emailErr.$fileErr);
}

?>