package com.itt.monitoring.cfg;

import com.google.gson.*;

public enum JsonType {
	
	OBJECT,
	PRIMITIVE,
	ARRAY;

	public static boolean isType(JsonType type, JsonElement element){

		switch(type){

			case OBJECT:
				return element.isJsonObject();
			case PRIMITIVE:
				return element.isJsonPrimitive();
			case ARRAY:
				return element.isJsonArray();

			default:
				return false;
		}
	}

	public static String toString(JsonType type){

		switch(type){

			case OBJECT:
				return "jsonObject";
			case PRIMITIVE:
				return "jsonPrimitive";
			case ARRAY:
				return "jsonArray";

			default:
				return "";
		}
	}
}