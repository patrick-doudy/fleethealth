SET appRoot=D:\Users\doudy\SDS\Monitoring\EmergingIssues\app

SET currDir=%cd%
SET action=%1

SET cfg0=%cfgRoot%\MonitorConfigNonHidden.json
SET cfg1=D:\Users\doudy\SDS\Monitoring\MonitorConfigHidden.json
SET cfg2=%cfgRoot%\TrendFamilies.json

cd %appRoot%

java -jar EmergingIssues.jar %cfg0% %cfg1% %cfg2% %action%

cd %currDir%