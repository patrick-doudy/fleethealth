SET libRoot=D:\Users\doudy\SDS\Monitoring\EmergingIssues\lib
SET cfgRoot=D:\Users\doudy\SDS\Monitoring\EmergingIssues\config
SET outRoot=D:\Users\doudy\SDS\Monitoring\EmergingIssues\classes

SET currDir=%cd%
SET action=%1

SET lcp0=%libRoot%\vertica-jdbc-8.1.1-18.jar
SET lcp1=%libRoot%\commons-math3-3.6.1.jar
SET lcp2=%libRoot%\gson-2.8.5.jar
SET lcp3=%libRoot%\sendgrid-java.jar

SET cfg0=%cfgRoot%\MonitorConfigNonHidden.json
SET cfg1=D:\Users\doudy\SDS\Monitoring\MonitorConfigHidden.json
SET cfg2=%cfgRoot%\TrendFamilies.json

cd %outRoot%

SET ccp=%outRoot%
SET runcp=%ccp%;%lcp0%;%lcp1%;%lcp2%;%lcp3%

java -cp %runcp% com.itt.monitoring.monitor.TrendMonitor %cfg0% %cfg1% %cfg2% %action%

cd %currDir%