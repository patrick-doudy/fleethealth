package com.itt.monitoring.trenddata;

import java.sql.*;
import java.util.*;
import java.io.*;

import com.itt.monitoring.query.TrendQuery;
import com.itt.monitoring.metric.Metric;
import com.itt.monitoring.cfg.*;
import com.itt.monitoring.util.*;

import com.google.gson.*;
import com.google.gson.stream.*;

public class TrendsContainer {

	/*
		Contain trends in Array list
	*/

	private static ArrayList<TrendFamily> trendFamilies = new ArrayList<TrendFamily>();

	/*
		As Singleton
	*/

	private static TrendsContainer singleton = new TrendsContainer();

	private TrendsContainer() {}

	public static TrendsContainer getInstance(){

		return singleton;
	}

	/*
		Methods
	*/

	private class TrendFamiliesJSON {

		JsonElement trendFamilies;
	}

	public static void readTrendConfigs(String trendConfigsPath){

		try {

			Gson gson = new Gson();
			JsonReader reader = new JsonReader(new FileReader(new File(trendConfigsPath)));
			TrendFamiliesJSON trendsJson = gson.fromJson(reader, TrendFamiliesJSON.class);

			ConfigValidator.checkIsArray("json file ","trendFamilies", trendsJson.trendFamilies);

			JsonArray trendsArray = trendsJson.trendFamilies.getAsJsonArray();

			for(int idx = 0; idx < trendsArray.size(); ++idx)
				trendFamilies.add(TrendFamily.fromJSON(trendsArray.get(idx)));

		} catch (Exception e){

			System.out.println("encountered exception "+e.getClass()+" while attempting to read configs");
			System.out.println(e.getMessage());
			System.out.println("aborting startup");
			System.exit(0);
		}
	}

	public static void computeTrends(Connection connection){

		for(int idx = 0; idx < trendFamilies.size(); ++idx){

			TrendFamily family = trendFamilies.get(idx);

			System.out.println("\ncomputing trend for " + family.getName()+"\n");

			family.evaluate(connection);
		}
	}

	public static String buildSummaryMessage(){

		String summaryMessage = "";

		summaryMessage += "Analysis performed on ";
		summaryMessage += TrendsUtil.getTodayString()+"\r\n\r\n";

		for(int idx = 0; idx < trendFamilies.size(); ++idx){

			TrendFamily family = trendFamilies.get(idx);

			summaryMessage += (idx == trendFamilies.size() -1)
							  ?  (family.getSummary() + "\r\n")
							  :  (family.getSummary());
		}

		return summaryMessage;
	}
}