<?php

require 'subscribe_util.php';

function remove_subscriber($email, $subscribers){

	for($row = 0; $row < count($subscribers); ++$row){

		if($subscribers[$row][COL_EMAIL] == $email){

			for($col = 0; $col < SUB_FIELDS; ++$col){
				unset($subscribers[$row][$col]);
			}

			unset($subscribers[$row]);
			$subscribers = array_values($subscribers);

			return $subscribers;
		}
	}

	return false;
}

if($argc != 2)
	exit;

$subscribers = read_subscribers();

$result = remove_subscriber($argv[1], $subscribers);

echo("remove result: ".$result);

?>