<?php

define("CONFIG_PATH", 	"C:\\inetpub\\wwwroot\\MonitorConfig.json");
define("SUB_DIR_KEY", 	"subscribersDir");
define("SUB_FN_KEY",	"subscribersFile");
define("SUB_TMP_FN", 	"tempSubPHP.csv");
define("UNSUB_TMP_FN", 	"tempUnsubPHP.csv");
define("SUB_FIELDS", 	3);
define("COL_EMAIL",  	0);
define("COL_FREQ",   	1);
define("COL_DATE",   	2);
define("CODE_SUCCESS",  "200");
define("CODE_FAIL", 	"999");

function get_sub_temp_path($fileName){


	if(($handle = fopen(CONFIG_PATH, "r")) !== false){

		$jsonStr = "";

		while(!feof($handle)){
			$jsonStr = $jsonStr.fgets($handle);
		}

		if(($configs = json_decode($jsonStr, true)) !== false){

			$dir  = $configs[SUB_DIR_KEY];
			$path = $dir.$fileName;

			return ($path);
		}

		return false;
	}

	return false;
}

function get_subscribers_path(){

	if(($handle = fopen(CONFIG_PATH, "r")) !== false){

		$jsonStr = "";

		while(!feof($handle)){
			$jsonStr = $jsonStr.fgets($handle);
		}

		if(($configs = json_decode($jsonStr, true)) !== false){

			$dir  = $configs[SUB_DIR_KEY];
			$fn   = $configs[SUB_FN_KEY];
			$path = $dir.$fn;

			return ($path);
		}

		return false;
	}

	return false;
}

function sanitize_input($data){
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

function get_email_err($postData){

	if(empty($postData["email"]))
		return 1;

	$inEmail = $postData["email"];

	if(!filter_var(
			$inEmail,
			FILTER_VALIDATE_EMAIL
		)
	){
		return 2;
	}

	return 0;
}

function read_subscribers(){

	/*
		Handle case of file not existing -
		sleep incase being handled by another
		process, then recreate
	*/

	if(!file_exists(get_subscribers_path()))
		sleep(2);
	if(!file_exists(get_subscribers_path())){
		$fh = fopen(get_subscribers_path(), "w");
		fclose($fh);
	}

	$subscribers = array();

	if(($subHandle = fopen(get_subscribers_path(), "r")) !== false){

		$row = 0;

		while(($data = fgetcsv($subHandle, 1024, ",")) !== false){
			for($col = 0; $col < SUB_FIELDS; ++$col){
				$subscribers[$row][$col] = trim($data[$col]);
			}
			++$row;
		}

		fclose($subHandle);

		return $subscribers;
	}

	return false;
}

function write_subscribers($subscribers,$fileName){

	if(($handle = fopen($fileName, "w")) !== false){

		for($row = 0; $row < count($subscribers); ++$row){
			for($col = 0; $col < SUB_FIELDS; ++$col){

				$str = $subscribers[$row][$col];

				if($col == SUB_FIELDS -1)
					$str = $str."\r\n";
				else
					$str = $str.", ";

				if(!fwrite($handle, $str))
					return false;
			}
		}

		fclose($handle);

		return true;
	}

	return false;
}

function subscriber_exists($email, $subscribers){

	for($row = 0; $row < count($subscribers); ++$row){

		if($subscribers[$row][COL_EMAIL] == $email){

			return true;
		}
	}

	return false;
}

function rotate_files($from, $to){

	if(rename($from, $to))
		return true;
	else
		return false;
}

?>