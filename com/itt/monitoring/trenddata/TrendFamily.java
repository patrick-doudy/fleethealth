package com.itt.monitoring.trenddata;

import java.sql.*;
import java.util.*;
import java.io.*;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

import com.itt.monitoring.query.TrendQuery;
import com.itt.monitoring.query.TwoDimensionCountByDateQuery;
import com.itt.monitoring.metric.Metric;
import com.itt.monitoring.metric.MovingAverageDelta;
import com.itt.monitoring.metric.RegressionSlope;
import com.itt.monitoring.cfg.*;

import com.google.gson.*;

public class TrendFamily {

	private String 		name 	  	= "";
	private TrendQuery 	trendQuery 	= null;
	private Metric 		metric    	= null;

	private ArrayList<TrendBaseData> familyBaseData = null;

	private TrendFamily (String name, TrendQuery trendQuery, Metric metric) {

		this.name 		= name;
		this.trendQuery = trendQuery;
		this.metric 	= metric;

		familyBaseData = new ArrayList<TrendBaseData>();
	}

	/*
		Trend Family from json
	*/

	private class TrendFamilyJSON {

		public String name;
		public JsonElement query;
		public JsonElement metric;
	}

	public static TrendFamily fromJSON(JsonElement jsonIn) {

		TrendFamily family = null;

		try{

			final String self	  	= "TrendFamily";
			final String beforeName = "json array member";

			ConfigValidator.checkIsObject(self, beforeName, jsonIn);

			Gson gson = new Gson();

			TrendFamilyJSON trendFamJson = gson.fromJson(jsonIn, TrendFamilyJSON.class);

			final String trendFamName = trendFamJson.name;

			ConfigValidator.checkIsObject(trendFamName, "query", trendFamJson.query);
			ConfigValidator.checkIsObject(trendFamName, "metric", trendFamJson.metric);

			JsonObject query  = trendFamJson.query.getAsJsonObject();
			JsonObject metric = trendFamJson.metric.getAsJsonObject();

			ConfigValidator.checkKeyValue(trendFamName, "query", query, "type", JsonType.PRIMITIVE);
			ConfigValidator.checkKeyValue(trendFamName, "metric", metric, "type", JsonType.PRIMITIVE);
			ConfigValidator.checkKeyValue(trendFamName, "query", query, "parameters", JsonType.OBJECT);
			ConfigValidator.checkKeyValue(trendFamName, "metric", metric, "parameters", JsonType.OBJECT);

			final String qPackage = "com.itt.monitoring.query.";
			final String mPackage = "com.itt.monitoring.metric.";

			Class<?> qClass = Class.forName(qPackage+query.get("type").getAsString());
			Class<?> mClass = Class.forName(mPackage+metric.get("type").getAsString());

			Method qFromJson = qClass.getMethod("fromJSON",JsonElement.class);
			Method mFromJson = mClass.getMethod("fromJSON",JsonElement.class);

			JsonElement qParams = query.get("parameters");
			JsonElement mParams = metric.get("parameters");

			TrendQuery 	qInstance = (TrendQuery) qFromJson.invoke(null,qParams);
			Metric 		mInstance = (Metric) 	 mFromJson.invoke(null,mParams);

			family = new TrendFamily(
				trendFamJson.name,
				qInstance,
				mInstance
			);

		} catch (InvocationTargetException e){

			Throwable cause = e.getCause();
			System.out.println("Encountered exception "+cause.getClass());
			System.out.println(cause.getMessage());
			System.out.println("aborting startup");
			System.exit(0);

		} catch (Exception e){

			System.out.println("Encountered exception "+e.getClass());
			System.out.println(e.getMessage());
			System.out.println("aborting startup");
			System.exit(0);
		}

		return family;
	}

	public String getName(){

		return name;
	}

	public String getSummary(){

		String summary = "\r\n" 
			+ getName() 
			+ " - " 
			+ this.metric.getWorstOffenderSummary();

		return summary;
	}

	public void evaluate(Connection connection){

		try {

			familyBaseData = trendQuery.performQuery(
				connection,
				this.metric.getEarliestDate()
			);

			this.metric.evaluate(familyBaseData);

			this.metric.printWorstOffenders();

		} catch (SQLException sqlException) {

			System.out.println("sql issue");
			System.out.println(sqlException.getMessage());
			System.out.println("aborting");
			System.exit(0);
		}
	}	
}