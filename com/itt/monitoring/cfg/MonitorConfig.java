package com.itt.monitoring.cfg;

import java.io.*;
import com.google.gson.*;
import com.google.gson.stream.*;

public class MonitorConfig {

	private static String dbConnString		= "";
	private static String dbConnUser 		= "";
	private static String dbConnPass 		= "";
	private static String sgApiKey 			= "";
	private static String reportUrl 		= "";
	private static String subscribeUrl 		= "";
	private static String analysesDir 		= "";
	private static String subscribersDir	= "";
	private static String subscribersFile 	= "";
	private static int    reportDaysRetain  = -1;
	private static int 	  maxReportsRetain  = -1;

	private class HiddenConfigsJson {

		public String dbConnString;
		public String dbConnUser;
		public String dbConnPass;
		public String sgApiKey;		
	}

	private class NonHiddenConfigsJson {

		public String reportUrl;
		public String subscribeUrl;
		public String analysesDir;
		public String subscribersDir;
		public String subscribersFile;
		public int    reportDaysRetain;
		public int    maxReportsRetain;
	}

	private static MonitorConfig singleton = new MonitorConfig();

	private MonitorConfig(){

	}

	public static void readAppConfigs(String nonHiddenPath, String hiddenPath) {

		try {

			Gson gson 			= new Gson();
			JsonReader reader 	= new JsonReader(new FileReader(new File(nonHiddenPath)));
			NonHiddenConfigsJson nonHidden = gson.fromJson(reader, NonHiddenConfigsJson.class);

			reportUrl 			= nonHidden.reportUrl;
			subscribeUrl		= nonHidden.subscribeUrl;
			analysesDir 		= nonHidden.analysesDir;
			subscribersDir 		= nonHidden.subscribersDir;
			subscribersFile 	= nonHidden.subscribersFile;
			reportDaysRetain 	= nonHidden.reportDaysRetain;
			maxReportsRetain 	= nonHidden.maxReportsRetain;

			reader = new JsonReader(new FileReader(new File(hiddenPath)));
			HiddenConfigsJson hidden = gson.fromJson(reader, HiddenConfigsJson.class);

			dbConnString 	= hidden.dbConnString;
			dbConnUser 		= hidden.dbConnUser;
			dbConnPass 		= hidden.dbConnPass;
			sgApiKey 		= hidden.sgApiKey;

		} catch (FileNotFoundException fnfException) {

			System.out.println("Encountered file not found exception");
			System.out.println(fnfException.getMessage());
			System.out.println("aborting startup");
			System.exit(0);
		}
	}

	public static String getDBConnString(){
		return dbConnString;
	}

	public static String getDBConnUser(){
		return dbConnUser;
	}

	public static String getDBConnPass(){
		return dbConnPass;
	}

	public static String getSgApiKey(){
		return sgApiKey;
	}

	public static String getReportUrl(){
		return reportUrl;
	}

	public static String getSubscribeUrl(){
		return subscribeUrl;
	}

	public static String getAnalysesDir(){
		return analysesDir;
	}

	public static String getSubscribersDir(){
		return subscribersDir;
	}

	public static String getSubscribersFilePath(){
		return (subscribersDir+subscribersFile);
	}

	public static int getReportDaysRetain(){
		return reportDaysRetain;
	}

	public static int getMaxReportsRetain(){
		return maxReportsRetain;
	}
}