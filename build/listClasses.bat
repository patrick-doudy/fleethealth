@ECHO OFF
setlocal disableDelayedExpansion
for /f "delims=" %%A in ('forfiles /s /m *.class /c "cmd /c echo @relpath"') do (
  set "file=%%~A"
  setlocal enableDelayedExpansion
  echo !file:~2!
  endlocal
)
@ECHO ON