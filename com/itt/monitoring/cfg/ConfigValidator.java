package com.itt.monitoring.cfg;

import com.google.gson.*;

public class ConfigValidator {

	public static void checkIsObject(String self, String objKey, JsonElement in)
		throws BadConfigException {

		if(!in.isJsonObject())
			throw new BadConfigException(self+" "+objKey+" key doesn't map to json object");
	}

	public static void checkIsArray(String self, String arrayKey, JsonElement in)
		throws BadConfigException {

		if(!in.isJsonArray())
			throw new BadConfigException(self+" "+arrayKey+" key doesn't map to json array");
	}

	public static void checkKeyValue(
		String self, String objName, JsonObject obj, String key, JsonType type)
		throws BadConfigException {

		if(!obj.has(key))
			throw new BadConfigException(self+" json object "+objName+" lacks key "+key);

		if(!JsonType.isType(type, obj.get(key)))
			throw new BadConfigException(self+" json object "+objName
				+" key "+key+" doesn't map to "+JsonType.toString(type));
	}
}