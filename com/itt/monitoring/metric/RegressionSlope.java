package com.itt.monitoring.metric;

import java.sql.*;
import java.util.*;
import java.lang.Math;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import com.itt.monitoring.trenddata.TrendBaseData;
import com.itt.monitoring.util.TrendsUtil;
import com.itt.monitoring.cfg.*;

import org.apache.commons.math3.stat.regression.SimpleRegression;

import com.google.gson.*;

public class RegressionSlope extends Metric {

	private int trendDays;
	private int numToReport;

	private double allowableNullFraction;
	private double minAvgCountPerDate;

	private ArrayList<RSTrend> worstOffenders = null;

	private Number inflateNullValue = new Integer(0);

	private RegressionSlope(
		int trendDays, int numToReport, 
		double allowableNullFraction, double minAvgCountPerDate) throws BadConfigException {

		if(!(trendDays > 0 && numToReport > 0))
			throw new BadConfigException("RegressionSlope trendDays and numToReport "
				+"must be greater than zero");

		if(!(allowableNullFraction >= 0.0 && allowableNullFraction < 1.0))
			throw new BadConfigException("RegressionSlope allowableNullFraction "
				+"must be greater than or equal to zero and less than one");

		if(!(minAvgCountPerDate >= 0.0))
			throw new BadConfigException("RegressionSlope minAvgCountPerDate "
				+"must be greater than or equal to zero");

		this.trendDays 				= trendDays;
		this.numToReport 			= numToReport;
		this.allowableNullFraction 	= allowableNullFraction;
		this.minAvgCountPerDate 	= minAvgCountPerDate;

		this.worstOffenders = new ArrayList<RSTrend>();
	}

	/*
		Regression Slope From JSON
	*/

	private class RegressionSlopeJSON {

		public int trendDays;
		public int numToReport;
		public double allowableNullFraction;
		public double minAvgCountPerDate;
	}

	public static RegressionSlope fromJSON(JsonElement jsonIn) throws BadConfigException {

		final String objName = "parameters";
		final String self    = "RegressionSlope";

		ConfigValidator.checkIsObject(self, objName, jsonIn);

		JsonObject paramsJson = jsonIn.getAsJsonObject();

		ConfigValidator.checkKeyValue(self, objName, paramsJson, "trendDays", JsonType.PRIMITIVE);
		ConfigValidator.checkKeyValue(self, objName, paramsJson, "numToReport", JsonType.PRIMITIVE);
		ConfigValidator.checkKeyValue(self, objName, paramsJson, "allowableNullFraction", JsonType.PRIMITIVE);

		Gson gson 	= new Gson();
		RegressionSlopeJSON jsonRs = gson.fromJson(jsonIn, RegressionSlopeJSON.class);

		return new RegressionSlope(
			jsonRs.trendDays,
			jsonRs.numToReport,
			jsonRs.allowableNullFraction,
			jsonRs.minAvgCountPerDate
		);
	}

	/*
		Metric Methods
	*/

	@Override
	public int getDaysOffset(){

		return this.trendDays;
	}

	@Override
	public java.sql.Date getEarliestDate(){

		Calendar calendar = TrendsUtil.cloneProgramCalendar();

		calendar.add(Calendar.DATE, - this.trendDays);

		return TrendsUtil.sqlDateFromCalendar(calendar);
	}

	@Override
	public Number getInflateNullValue(){

		return this.inflateNullValue;
	}

	@Override
	public void evaluate(ArrayList<TrendBaseData> baseDataList){

		for(int idx = 0; idx < baseDataList.size(); ++idx){

			TrendBaseData baseData = null;

			RSTrend rsTrend = null;

			baseData = baseDataList.get(idx);

			baseData.inflate(
				this.getDaysOffset(),
				this.getInflateNullValue()
			);

			if(baseData.getNullFraction() > allowableNullFraction
			|| baseData.getAvgCountPerDate() < minAvgCountPerDate){

				baseDataList.set(idx, null);
				baseData = null;
				rsTrend  = null;
				
				continue;
			}

			rsTrend = new RSTrend(
				this.trendDays,
				baseData
			);

			if(rsTrend.getNormalizedSlope() <= 0.0){

				baseDataList.set(idx, null);
				baseData = null;
				rsTrend  = null;
				
				continue;
			}

			if(storeIfWorstOffender(rsTrend)){

				//rsTrend.writeCSV();

			} else {

				baseDataList.set(idx, null);
				rsTrend = null;
			}
		}

		Collections.sort(worstOffenders, Collections.reverseOrder());
	}

	@Override 
	public void printWorstOffenders(){

		System.out.print(getWorstOffenderSummary());
	}

	@Override
	public String getWorstOffenderSummary(){

		String message = "Regression Slope Worst "+numToReport
			+" Offenders Last "+trendDays+" Days:\r\n\r\n";

		for(int idx = 0; idx < worstOffenders.size(); ++idx){

			RSTrend offender = worstOffenders.get(idx);

			message += offender.getDimensionName()+"\r\n";

			// message += "DIM1: "+offender.getDimension1()
			// 	+" DIM2: "+offender.getDimension2()
			// 	+" Normalized Slope: "+offender.getNormalizedSlope()
			// 	+" Nominal Slope: "+offender.getNominalSlope()
			// 	+" Average per Date: "+offender.getAvgCountPerDate()
			// 	+"\n";
		}

		return message;
	}

	private boolean storeIfWorstOffender(RSTrend rsTrend){

		if(worstOffenders.size() < numToReport){

			worstOffenders.add(rsTrend);

			return true;

		} else {

			int minIdx 		= -1;
			double minSlope = Double.MAX_VALUE;

			for(int idx = 0; idx < worstOffenders.size(); ++idx){

				double slope = worstOffenders.get(idx).getNormalizedSlope();

				if(slope < minSlope){
					minSlope = slope;
					minIdx   = idx;
				}
			}

			if(rsTrend.getNormalizedSlope() > minSlope){

				worstOffenders.set(minIdx, rsTrend);

				return true;
			}
		}

		return false;
	}

	private class RSTrend implements Comparable<RSTrend> {

		private String dimension1 = "";
		private String dimension2 = "";

		private int trendDays;

		private double avgCountPerDate  	= 0.0;

		private java.sql.Date[] dates		= null;
		private double[] nominalValues 		= null;
		private double[] normalizedValues 	= null;

		private double nominalSlope 		= 0.0;
		private double normalizedSlope 		= 0.0;
		private double nominalIntercept 	= 0.0;
		private double normalizedIntercept  = 0.0;

		public RSTrend(int trendDays, TrendBaseData baseData){

			this.dimension1 = baseData.getDimension1();
			this.dimension2 = baseData.getDimension2();

			this.trendDays  = trendDays;

			this.avgCountPerDate = baseData.getAvgCountPerDate();

			/*
				copy out base data and normalize
			*/ 

			int baseEntries 			= baseData.getBaseDates().length;
			java.sql.Date[] baseDates 	= baseData.getBaseDates();
			Number[] baseValues 		= baseData.getBaseValues();

			dates 			 = new java.sql.Date[baseEntries];
			nominalValues 	 = new double[baseEntries];
			normalizedValues = new double[baseEntries];

			double nominalMax = -Double.MAX_VALUE;

			for(int idx = 0; idx < baseEntries; ++idx){

				dates[idx] 			= baseDates[idx];
				nominalValues[idx] 	= baseValues[idx].doubleValue();

				if(nominalValues[idx] > nominalMax && nominalValues[idx] > 0.0)
					nominalMax = nominalValues[idx];
			}

			if(nominalMax == -Double.MAX_VALUE)
				nominalMax = 1.0;

			for(int idx = 0; idx < baseEntries; ++idx)
				normalizedValues[idx] = nominalValues[idx] / nominalMax;

			/*
				compute regression slopes
			*/

			SimpleRegression nomRegression  = new SimpleRegression();
			SimpleRegression normRegression = new SimpleRegression();

			for(int idx = 0; idx < baseEntries; ++idx){
				nomRegression.addData(idx, nominalValues[idx]);
				normRegression.addData(idx, normalizedValues[idx]);
			}

			nominalSlope    	= nomRegression.getSlope();
			normalizedSlope 	= normRegression.getSlope();
			nominalIntercept 	= nomRegression.getIntercept();
			normalizedIntercept = normRegression.getIntercept();

			nomRegression  = null;
			normRegression = null;
		}

		@Override 
		public int compareTo(RSTrend other){

			double slopeOther = other.getNormalizedSlope();

			if(normalizedSlope > slopeOther)
				return 1;
			else if (normalizedSlope == slopeOther)
				return 0;
			else
				return -1;
		}

		public String getDimension1(){

			return dimension1;
		}

		public String getDimension2(){

			return dimension2;
		}

		public double getNominalSlope(){

			return nominalSlope;
		}

		public double getNormalizedSlope(){

			return normalizedSlope;
		}

		public double getAvgCountPerDate(){

			return avgCountPerDate;
		}

		public double getPercChange(){

			/*	
				Take appx percentage change as ratio of last day regression
				line output to first day regression line output
			*/

			double first = nominalIntercept;
			double last  = nominalIntercept + nominalSlope * (trendDays -1);

			if(last >= first && first == 0.0)
				first = 1.0;

			if(first >= last && last == 0.0)
				last  = 1.0;

			double delta = 	(last >= first)
						 ? 	(1 - first / last)
						 : -(1 - last / first);

			return Math.floor(100 * delta);
		}

		public String getDimensionName(){

			return (dimension1+" vs "+dimension2);
		}

		public String getPercChangeMsg(){

			String message = dimension1+" vs "+dimension2
				+" "+getPercChange()+"% change";

			return message;
		}

		public void writeCSV(){

			String fileName = dimension1+"vs"+dimension2+".csv";

			try{

				File csvFile = new File(fileName);

				csvFile.delete();

				PrintWriter printWriter = new PrintWriter(csvFile);

				StringBuilder builder = new StringBuilder();

				builder.append("date, nominalVal, normalizedVal\n");

				for(int idx = 0; idx < normalizedValues.length; ++idx){

					builder.append(dates[idx].toString()+", "+nominalValues[idx]+", "
						+normalizedValues[idx]+"\n");
				}

				printWriter.write(builder.toString());

				printWriter.close();

			} catch (FileNotFoundException FNFException){

				System.out.println("Could not write csv for "+fileName);

				return;
			}
		}
	}
}

